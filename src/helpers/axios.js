import { LocalStorage } from 'quasar'
import axios from 'axios'
import store from '../store'

/**
 * createUser function
 * creates a new user
 */
export function createUser () {
  return axios({
    method: 'post',
    url: process.env.API_URL + '/api/register',
    responseType: 'json',
    data: {}
  })
}

/**
 * fetchUser function
 * gets an existing user
 */
export function fetchUser () {
  return axios({
    method: 'get',
    url: process.env.API_URL + '/api/me',
    responseType: 'json',
    headers: {
      'Authorization': 'Bearer ' + LocalStorage.getItem('api_token')
    }
  })
}

/**
 * getBrands function
 */
export function getBrands () {
  return axios({
    method: 'get',
    url: process.env.API_URL + '/api/brands',
    responseType: 'json',
    headers: {
      'Authorization': 'Bearer ' + LocalStorage.getItem('api_token')
    }
  })
}

/**
 * setBrands function
 */
export function setBrands (data) {
  return axios({
    method: 'post',
    url: process.env.API_URL + '/api/brands/user',
    responseType: 'json',
    data: data,
    headers: {
      'Authorization': 'Bearer ' + LocalStorage.getItem('api_token')
    }
  })
}

/**
 * getItem
 */
export function getItem () {
  return axios({
    method: 'get',
    url: process.env.API_URL + '/api/items/get-random',
    responseType: 'json',
    headers: {
      'Authorization': 'Bearer ' + LocalStorage.getItem('api_token')
    }
  })
}

/**
 * getItems
 */
export function getItems (qty) {
  return axios({
    method: 'get',
    url: process.env.API_URL + '/api/items/get-random/' + qty,
    responseType: 'json',
    headers: {
      'Authorization': 'Bearer ' + LocalStorage.getItem('api_token')
    }
  })
}

/**
 * updateItem
 */
export function updateItem (direction, itemId, data = null) {
  return axios({
    method: 'post',
    url: process.env.API_URL + '/api/items/' + itemId + '/swipe',
    responseType: 'json',
    data: {
      direction,
      data
    },
    headers: {
      'Authorization': 'Bearer ' + LocalStorage.getItem('api_token')
    }
  })
}

/**
 *
 */
export function getRemainingItems (total) {
  let itemsGot = store.state.items.items.length
  return axios({
    method: 'get',
    url: process.env.API_URL + '/api/items/get-random/' + (total - itemsGot),
    responseType: 'json',
    headers: {
      'Authorization': 'Bearer ' + LocalStorage.get.item('api_token')
    }
  })
}

/**
 * user items
 */
export function getUserItems (direction = 0) {
  return axios({
    method: 'get',
    url: process.env.API_URL + '/api/users/items',
    responseType: 'json',
    headers: {
      'Authorization': 'Bearer ' + LocalStorage.getItem('api_token')
    },
    params: {
      direction: direction
    }
  })
}
