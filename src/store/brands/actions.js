/*
export function someAction (context) {
}
*/

export function setBrands (context, data) {
  context.commit('setBrands', data)
}

export function selectBrand (context, data) {
  context.commit('selectBrand', data)
}
