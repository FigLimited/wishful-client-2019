/*
export function someMutation (state) {
}
*/

export function setBrands (state, data) {
  state.items = data
}

export function selectBrand (state, data) {
  state.items[data.id].selected = !state.items[data.id].selected
}
