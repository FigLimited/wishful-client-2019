/*
export function someAction (context) {
}
*/

export function addItems (context, data) {
  context.commit('addItems', data)
}

export function setItems (context, data) {
  context.commit('setItems', data)
}

export function removeItem (context, data) {
  context.commit('removeItem', data)
}

export function setLiked (context, data) {
  context.commit('setLiked', data)
}

export function setDisliked (context, data) {
  context.commit('setDisliked', data)
}

export function setShared (context, data) {
  context.commit('setShared', data)
}

export function setReported (context, data) {
  context.commit('setReported', data)
}
