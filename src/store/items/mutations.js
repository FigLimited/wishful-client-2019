/*
export function someMutation (state) {
}
*/

export function addItems (state, data) {
  let local = state.items.map(({ id }) => id)

  data.forEach(function (element) {
    if (local.indexOf(element.id) === -1) {
      state.items.push(element)
    }
  })
}

export function setItems (state, data) {
  state.items = data
}

export function removeItem (state, data) {
  // console.log('MUTATION', data)
  for (var i = state.items.length - 1; i >= 0; --i) {
    if (state.items[i].id === data.itemId) {
      state.items.splice(i, 1)
    }
  }
}

export function setLiked (state, data) {
  state.liked = data
}

export function setDisliked (state, data) {
  state.disliked = data
}

export function setReported (state, data) {
  state.reported = data
}

export function setShared (state, data) {
  state.shared = data
}
