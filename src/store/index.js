import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

// we first import the module
import user from './user'
import brands from './brands'
import items from './items'
import settings from './settings'

const store = new Vuex.Store({
  modules: {
    // then we reference it
    user,
    brands,
    items,
    settings
  }
})

export default store
