
const routes = [
  {
    path: '/',
    component: () => import('layouts/Vuex.vue'),
    children: [
      { path: '', component: () => import('pages/Index'), name: 'home' },
      { path: 'item/:id/links', component: () => import('pages/Items/Links'), name: 'links' }
    ]
  },
  {
    path: '/brands',
    component: () => import('layouts/Brands'),
    children: [
      { path: '', component: () => import('pages/Brands/Index'), name: 'brands' }
    ]
  },
  {
    path: '/feed',
    component: () => import('layouts/Feed'),
    children: [
      { path: '', component: () => import('pages/Items/Feed'), name: 'feed' }
    ]
  },
  {
    path: '/bookmarks',
    component: () => import('layouts/Users.vue'),
    children: [
      // { path: 'liked', component: () => import('pages/Bookmarks/Liked.vue'), name: 'bookmarks.liked' },
      // { path: 'disliked', component: () => import('pages/Bookmarks/Disliked.vue'), name: 'bookmarks.disliked' },
      { path: '', component: () => import('pages/Users/Bookmarks.vue'), name: 'bookmarks' }
      // { path: 'reported', component: () => import('pages/Bookmarks/liked.vue'), name: 'reported' },
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
